$(document).ready(function () {
    $('#commonprescript').hide();
    $('#route').hide();
    /*	$('#forms').hide();
    	$('#freqstrength').hide();
    	$('#dosage').hide();
    	$('#freq').hide();
    	$('#qid').hide();*/

    //		$('#prescription2').addClass('visible-xs');


    /* Expand all/Collapse all Script */
    $('.closeall').click(function () {
        $(this).parent().siblings('[id*="accordion"]').find('h3.panel-title').children().addClass('collapsed');
        $(this).parent().siblings('[id*="accordion"]').find('.panel-collapse').removeClass('in');
        $(this).parent().siblings('[id*="accordion"]').find('.panel-collapse').css('height', 'auto');
        $(this).parent().siblings('[id*="accordion"]').find('.panel-collapse').attr('aria-expanded', 'false');
    });
    $('.openall').click(function () {
        $(this).parent().siblings('[id*="accordion"]').find('h3.panel-title').children().removeClass('collapsed');
        $(this).parent().siblings('[id*="accordion"]').find('.panel-collapse').addClass('in');
        $(this).parent().siblings('[id*="accordion"]').find('.panel-collapse').css('height', 'auto');
        $(this).parent().siblings('[id*="accordion"]').find('.panel-collapse').attr('aria-expanded', 'true');
    });
    /* Expand all/Collapse all Script */


    /*	$(window).on("beforeunload", function () {
    	    $('#seldiagnosis option').prop('selected', function() {
    	        return this.defaultSelected;
    	    });
    	});

    	$('#seldiagnosis').change(function(){

    		if($(this).val() == 1){
    			$('#prescription1,#prescription2').hide();
    			$('#commonprescript').show();
    			$('#prescription2').removeClass('visible-xs');
    		}
    		if($(this).val() != 1){
    	    	alert('Wrong selection or No data available');
    	    	$('.panel-collapse.collapse').removeClass('in');
    			$('#prescription1,#prescription2').show();
    			$('#route').hide();
    			$('#prescription2').addClass('visible-xs');
    		    $('#seldiagnosis option').prop('selected', function() {
    		        return this.defaultSelected;
    		    });
    	    }

    		});
    	$('#drug').on('autocompleteselect', function(){
    		if($(this).val() == "Lasix" && $('#seldiagnosis').val() == 1){
    			$('#prescription1,#prescription2').hide();
    			$('#commonprescript').hide();
    			$('#route').show();
    			$('#prescription2').removeClass('visible-xs');
    	    }
    	    else {
    	    	alert('Selection mismatch or No data available.');
    	    	$('.panel-collapse.collapse').removeClass('in');
    			$('#prescription1,#prescription2').show();
    			$('#route').hide();
    			$('#prescription2').addClass('visible-xs');
    			$(this).attr("value", " ");
    		    $('#seldiagnosis option').prop('selected', function() {
    		        return this.defaultSelected;
    		    });
    	    }

    		});*/
});

document.addEventListener("DOMContentLoaded", function (event) {

    var g1 = new JustGage({
        id: 'g1',
        value: 25,
        min: 0,
        max: 100,
        pointer: true,
        gaugeWidthScale: 0.6,
        textRenderer: customValue,
        counter: true,
        startAnimationTime: 2000,
        startAnimationType: "<>",
        refreshAnimationTime: 1000,
        refreshAnimationType: "<>",
        customSectors: [{
            color: '#ff0000',
            lo: 50,
            hi: 100
            }, {
            color: '#00ff00',
            lo: 0,
            hi: 50
            }],
        counter: true
    });

    var i = 0;


    setInterval(function () {
        if (i >= stepsArray.length) i = 0;
        i++;
        if (stepsArray[i] == 160) {
            g1.refresh(stepsArray[i]);
        }
    }, 500);


    function customValue(val) {
        if (val < 33) {
            return 'Low';
        } else if (val > 66) {
            return 'High';
        } else {
            return 'Medium';
        }
    }
});
